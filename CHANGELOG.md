# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## [Unreleased]

## [0.2.1] - 2019-07-17
### Changed

* Renamed distribution `attrs-patch` &rarr; `pedroasad-attrs-patch` for publishing on [PyPI].

## [0.2.0] - 2019-07-17
### Changed

* Upload documentation builds on pushes to `master`.
* Changed package name `attr_patch` &rarr; `attrs_patch.attr`; docs updated accordingly.

## [0.1.0] - 2019-07-16
### Added

* [`.bumpversion.cfg`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion] version-tagging package.
* [`.coveragerc`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/.coveragerc) &mdash; Configuration file for the [Coverage] reporting tool.
* [`.flake8`](https://gitlab.com/psa-exe/python-attrs-patch/blob/master/.flake8) &mdash; Configuration for [flake8] linting (complements [Black] config in `pyproject.toml`.
* [`.gitignore`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI]), configured for:
  * Running a [pytest]-based test suite and reporting results with [Codecov] at https://codecov.io/gl/psa-exe/python-attrs-patch,
  * Building the [Sphinx]-based documentation and deploying it via [Gitlab Pages] to https://psa-exe.gitlab.io/python-attrs-patch, and
  * Uploading successfully built, tagged distributions to [TestPyPI] (defaults to https://test.pypi.org/project/attrs-patch).
* [`.pre-commit-config.yaml`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/.pre-commit-config.yaml) &mdash; Configuration file for the [pre-commit] package, which aids in applying useful [Git Hooks] in team workflows. Includes:
  * Automatic code styling with [autopep8].
* [`CHANGELOG.md`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/CHANGELOG.md) &mdash; this very history file, which follows the [Keep a Changelog] standard.
* [`LICENSE.txt`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/LICENSE.txt) &mdash; Copy of the [MIT License].
* [`MANIFEST.md`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/MANIFEST.md) Repository's manifest.
* [`README.md`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/README.md) &mdash; repository front-page.
* [`docs/`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/docs) &mdash; [Sphinx]-based documentation setup, which includes:
  * [`conf.py`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/docs/conf.py) &mdash; [Sphinx] configuration file,
  * [`docs/index.rst`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/docs/index.rst) &mdash; documentation master file, and
  * [`Makefile`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/docs/Makefile) for building the docs easily.
* [`poetry.lock`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/poetry.lock) &mdash; [Poetry]'s resolved dependency file. Whereas [`pyproject.toml`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/pyproject.toml) &mdash; [PEP-517]-compliant packaging metadata, configured with the [Poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py] file found in *classical* Python packaging.
* [`src/attr_patch`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/attr_patch) &mdash; Base directory of the example Python package distributed by this repository.
* [`tests`](https://gitlab.com/psa-exe/python-attrs-patch/blob/0.1.0/tests) &mdash; [pytest]-powered test-suite.

[LICENSE]: https://gitlab.com/psa-exe/python-attrs-patch/blob/master/LICENSE.txt
[MANIFEST]: https://gitlab.com/psa-exe/python-attrs-patch/blob/master/MANIFEST.md
[README]: https://gitlab.com/psa-exe/python-attrs-patch/blob/master/README.md
[Unreleased]: https://gitlab.com/psa-exe/python-attrs-patch/compare?from=release&to=master
[0.2.1]: https://gitlab.com/psa-exe/python-attrs-patch/compare?from=0.2.0&to=0.2.1
[0.2.0]: https://gitlab.com/psa-exe/python-attrs-patch/compare?from=0.1.0&to=0.2.0
[0.1.0]: https://gitlab.com/psa-exe/python-attrs-patch/tags/0.1.0

[Black]: https://black.readthedocs.io/en/stable/
[Codecov]: https://codecov.io
[Coverage]: https://coverage.readthedocs.io
[Git hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[Gitlab CI]: https://docs.gitlab.com/ee/ci
[Gitlab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[MIT License]: https://opensource.org/licenses/MIT
[PEP-517]: https://www.python.org/dev/peps/pep-0517/
[Poetry]: https://github.com/sdispater/poetry
[Poetry]: https://github.com/sdispater/poetry
[PyPI]: https://pypi.org
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Sphinx]: https://www.sphinx-doc.org/en/master/index.html
[TestPyPI]: https://test.pypi.org
[autopep8]: https://pypi.org/project/autopep8/
[bumpversion]: https://github.com/peritus/bumpversion
[flake8]: http://flake8.pycqa.org/en/latest/
[gitignore]: https://git-scm.com/docs/gitignore
[open-source]: https://opensource.org/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[pytest]: https://pytest.org/
[repository-codecov]: https://codecov.io/gl/psa-exe/python-attrs-patch
[setup.py]: https://docs.python.org/3.5/distutils/setupscript.html

