================================================================
attrs_patch --- A set of patches for the excellent attrs library
================================================================

.. toctree::
   :maxdepth: 2

   Get started <README>
   api
   CHANGELOG
   CONTRIBUTING
   MANIFEST


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

