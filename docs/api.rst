=============
API reference
=============

:mod:`attrs_patch.attr`
=======================

.. automodule:: attrs_patch.attr
    :members:

:mod:`attrs_patch.attr.converters`
==================================

.. automodule:: attrs_patch.attr.converters
    :members:

:mod:`attrs_patch.attr.validators`
==================================

.. automodule:: attrs_patch.attr.validators
    :members:

