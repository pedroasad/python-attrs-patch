from attrs_patch import attr
import textwrap


def test_autodoc():
    @attr.autodoc
    @attr.s
    class SomeClass:
        """Introductory line of class docstring.

        Additional docstring content.
        """

        _a: int = attr.ib(metadata={"help": "Attribute a help text."})
        b: float = attr.ib(
            default=8.5, metadata={"help": "Attribute b help text."}
        )

    docstring = textwrap.dedent(
        """\
        Introductory line of class docstring.

        Additional docstring content.

        Parameters
        ----------
        a: int
            Attribute a help text.

        b: float, optional
            Attribute b help text. This value is accessible, after initialization, via the ``b`` attribute."""
    )

    assert SomeClass.__doc__ == docstring
